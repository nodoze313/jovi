jovi
====

OviPets Grease Monkey script providing enhanced UI

**Using jovi is against the "OviPets Code of Conduct"**

 The double-click feed all tabs method is the only method 
 that runs faster than a human, so you might not want to use it.

That we know of, no one has ever been banned from OviPets for using this.

Provides:
- tab mass feeder
- starving pet patrol
- automatic egg turning
- list randomizer
- random number generator
- resurrector (only works if you have the credits to do the resurrection)

To use:
- Firefox is recommended, Chrome is also tested working properly.
  - https://www.mozilla.org/en-US/firefox/new/
- Install Greasemonkey or Tampermonkey
  - Firefox:
    - https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/
  - Chrome:
    - https://chrome.google.com/webstore/detail/tampermonkey/
- Go to:
  - https://greasyfork.org/scripts/867-jovi
  - Click Install button


Please submit bugs at:
https://github.com/nodoze313/jovi/issues

Submissions are welcome, unified patches or push.

