module.exports = function(grunt) {

  grunt.initConfig({
    watch: {
      files: ['<%= jshint.files %>'],
      tasks: ['jshint']
    },

    dist: {
        // the files to concatenate
        src: ['jovi.user.js'],
        // the location of the resulting JS file
        dest: 'jovi.js'
    },
    jshint: {
      // define the files to lint
      files: ['Gruntfile.js', 'jovi.user.js'],
      // configure JSHint (documented at http://www.jshint.com/docs/)
      options: {
        // more options here if you want to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true
        }
      }
    }
  });


  grunt.registerTask('default', ['jshint']);

};

